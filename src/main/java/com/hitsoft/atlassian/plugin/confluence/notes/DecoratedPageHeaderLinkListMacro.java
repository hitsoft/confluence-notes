package com.hitsoft.atlassian.plugin.confluence.notes;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: smeagol
 * Date: 22.08.12
 * Time: 11:34
 */
public class DecoratedPageHeaderLinkListMacro implements Macro {

    private static final String TEMPLATE = "com/hitsoft/atlassian/plugin/confluence/notes/DecoratedPageHeaderLinkListMacro.vm";

    private final PageManager pageManager;
    private final SpaceManager spaceManager;
    private final Renderer renderer;
    private final XhtmlContent xhtmlContent;

    public static class Link {
        public String prefix;
        public String url;
        public boolean isUrl;

        public String getPrefix() {
            return prefix;
        }

        public String getUrl() {
            return url;
        }

        public boolean getIsUrl() {
            return isUrl;
        }
    }

    public static class Options {
        public boolean isOrderedList = false;
        public boolean isUnorderedList = false;
        public boolean isDecoratedPreText = false;
        public String decorationPreText;

        public boolean getIsOrderedList() {
            return isOrderedList;
        }

        public boolean getIsUnorderedList() {
            return isUnorderedList;
        }

        public boolean getIsDecoratedPreText() {
            return isDecoratedPreText;
        }

        public String getDecorationPreText() {
            return decorationPreText;
        }
    }

    public DecoratedPageHeaderLinkListMacro(PageManager pageManager, SpaceManager spaceManager, Renderer renderer, XhtmlContent xhtmlContent) {
        this.pageManager = pageManager;
        this.spaceManager = spaceManager;
        this.renderer = renderer;
        this.xhtmlContent = xhtmlContent;
    }

    public static class Prefixes extends HashMap<String, String> {
        public Prefixes(String data) {
            super();
            for (String row : data.split("\n")) {
                String[] items = row.split("=");
                put(items[0], row.substring(items[0].length() + 1));
            }
        }

        @Override
        public String get(Object o) {
            String res = super.get(o);
            if (res == null)
                res = "";
            return res;
        }
    }

    @Override
    public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException {
        StringBuilder error = new StringBuilder();
        Options options = new Options();
        options.isOrderedList = "ordered-list".equals(params.get("decoration"));
        options.isUnorderedList = "unordered-list".equals(params.get("decoration"));
        options.isDecoratedPreText = "pre-text".equals(params.get("decoration"));
        options.decorationPreText = params.get("decoration-pre-text");
        Prefixes prefixes = new Prefixes(params.get("prefixes"));

        List<Link> links = new ArrayList<Link>();
        for (String row : body.split("\n")) {
            String[] items = row.split("=");
            Link link = new Link();
            if (items.length > 1) {
                link.isUrl = true;
                link.prefix = prefixes.get(items[0]);
                link.url = row.substring(items[0].length() + 1);
            } else {
                link.isUrl = false;
                link.prefix = null;
                link.url = row;
            }
            links.add(link);
        }
        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();
        ctx.put("links", links);
        ctx.put("options", options);
        ctx.put("error", error.toString());
        return renderer.render(VelocityUtils.getRenderedTemplate(TEMPLATE, ctx), conversionContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
