package com.hitsoft.atlassian.plugin.confluence.notes;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.HtmlElementIdCreator;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.xhtml.api.XhtmlVisitor;

import javax.xml.stream.events.XMLEvent;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: smeagol
 * Date: 21.08.12
 * Time: 11:58
 */
public class DecoratedPageHeaderLinkMacro implements Macro {

    // We just have to define the variables and the setters, then Spring injects the correct objects for us to use. Simple and efficient.
    // You just need to know *what* you want to inject and use.

    private static final String TEMPLATE = "com/hitsoft/atlassian/plugin/confluence/notes/DecoratedPageHeaderLinkMacro.vm";
    private static final String NL = "\n";

    private final PageManager pageManager;
    private final SpaceManager spaceManager;
    private final Renderer renderer;
    private final XhtmlContent xhtmlContent;

    public DecoratedPageHeaderLinkMacro(PageManager pageManager, SpaceManager spaceManager, Renderer renderer, XhtmlContent xhtmlContent) {
        this.pageManager = pageManager;
        this.spaceManager = spaceManager;
        this.renderer = renderer;
        this.xhtmlContent = xhtmlContent;
    }

    class ParsedUrl {

        Pattern pByIdFull = Pattern.compile("^http.*\\?pageId=(\\d+)#(.*)$");
        Pattern pByIdFullNoAnchor = Pattern.compile("^http.*\\?pageId=(\\d+)$");
        Pattern pByIdPartial = Pattern.compile("^(\\d+)#(.*)$");
        Pattern pByIdPartialNoAnchor = Pattern.compile("^(\\d+)$");

        Pattern pByTitleFull = Pattern.compile("^http.*/display/([^/]+)/(.*)#(.*)$");
        Pattern pByTitleFullNoAnchor = Pattern.compile("^http.*/display/([^/]+)/(.*)$");
        Pattern pByTitlePartial = Pattern.compile("^([^/]+)/(.*)#(.*)$");
        Pattern pByTitlePartialNoAnchor = Pattern.compile("^([^/]+)/(.*)$");

        final ConversionContext conversionContext;

        ContentEntityObject page;
        String anchor;

        ParsedUrl(ConversionContext conversionContext) {
            this.conversionContext = conversionContext;
        }

        boolean parse(String url) {
            return parseById(url) || parseByTitle(url);
        }

        private boolean parseByTitle(String url) {
            boolean result = false;
            Matcher m = pByTitleFull.matcher(url);
            if (!m.matches()) {
                m = pByTitleFullNoAnchor.matcher(url);
                if (!m.matches()) {
                    m = pByTitlePartial.matcher(url);
                    if (!m.matches())
                        m = pByTitlePartialNoAnchor.matcher(url);
                }
            }
            if (m.matches()) {
                Space space = spaceManager.getSpace(m.group(1));
                if (space != null) {
                    page = pageManager.getPage(space.getKey(), URLDecoder.decode(m.group(2)));
                    if (page != null) {
                        anchor = parseAnchor((m.groupCount() > 2) ? m.group(3) : "");
                        result = anchor != null;
                    }
                }
            }
            return result;
        }

        private boolean parseById(String url) {
            boolean result = false;
            Matcher m = pByIdFull.matcher(url);
            if (!m.matches()) {
                m = pByIdFullNoAnchor.matcher(url);
                if (!m.matches()) {
                    m = pByIdPartial.matcher(url);
                    if (!m.matches())
                        m = pByIdPartialNoAnchor.matcher(url);
                }
            }
            if (m.matches()) {
                page = pageManager.getById(Long.parseLong(m.group(1)));
                if (page != null) {
                    anchor = parseAnchor((m.groupCount() > 1) ? m.group(2) : "");
                    result = anchor != null;
                }
            }
            return result;
        }

        private String parseAnchor(String anchor) {
            String result = null;
            HtmlElementIdCreator idCreator = new HtmlElementIdCreator();
            List<String> headers = getHeaders(page, conversionContext);
            for (String header: headers) {
                String id = idCreator.generateId(page.getTitle() + "-" + header);
                if (id.equals(anchor)) {
                    result = header;
                    break;
                }
            }
            return result;
        }
    }

    /**
     * This method returns XHTML to be displayed on the page that uses this macro
     * we just do random stuff here, trying to show how you can access the most basic
     * managers and model objects. No emphasis is put on beauty of code nor on
     * doing actually useful things :-)
     */
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        String prefixParam = parameters.get("prefix");
        String urlParam = parameters.get("url");
        String pageParam = parameters.get("page");
        String headerParam = parameters.get("header");

        StringBuilder error = new StringBuilder();
        StringBuilder caption = new StringBuilder(prefixParam);
        LinkModel link = new LinkModel();
        ContentEntityObject page;
        if (urlParam != null) {
            ParsedUrl url = new ParsedUrl(context);
            if (url.parse(urlParam)) {
                page = url.page;
                link.anchor = url.anchor;
            } else {
                page = null;
                error.append("Page not found by URL '").append(urlParam).append("'.").append(NL);
            }
        } else {
            page = null;
            if (pageParam == null)
                error.append("Page not specified").append(NL);
            else {
                String space = pageParam.split(":")[0];
                page = pageManager.getPage(space, pageParam.substring(space.length() + 1));
                link.anchor = findHeaderByTitle(page, headerParam, context);
                if (page == null)
                    error.append("Page '").append(pageParam).append("' not found").append(NL);
            }
        }
        if (page != null) {
            link.space = spaceManager.getSpaceFromPageId(page.getId());
            link.title = page.getTitle();
            caption.append(page.getTitle());
            if (!link.anchor.isEmpty())
                caption.append(", ").append(link.anchor);
            link.caption = caption.toString();
        }
        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();
        ctx.put("link", link);
        ctx.put("error", error.toString());
        return renderer.render(VelocityUtils.getRenderedTemplate(TEMPLATE, ctx), context);
    }

    @RequiresFormat(Format.Storage)
    private String findHeaderByTitle(ContentEntityObject page, String param, ConversionContext conversionContext) {
        String result = "";
        List<String> headers = getHeaders(page, conversionContext);
        String eq = null;
        String eqNoCase = null;
        String match = null;
        String contain = null;
        for (String header : headers) {
            if (eq != null && header.equals(param))
                eq = header;
            if (eqNoCase != null && header.equalsIgnoreCase(param))
                eqNoCase = header;
            if (match != null && header.matches(param))
                match = header;
            if (contain != null && header.contains(param))
                contain = header;
        }
        if (eq != null)
            result = eq;
        else if (eqNoCase != null)
            result = eqNoCase;
        else if (match != null)
            result = match;
        else if (contain != null)
            result = contain;
        return result;
    }

    private List<String> getHeaders(ContentEntityObject page, ConversionContext conversionContext) {
        final List<String> result = new ArrayList<String>();
        List<XhtmlVisitor> visitors = new ArrayList<XhtmlVisitor>();
        visitors.add(new XhtmlVisitor() {
            boolean inHeader;
            StringBuilder sb = new StringBuilder();

            @Override
            public boolean handle(XMLEvent xmlEvent, ConversionContext context) {
                if (xmlEvent.isStartElement()) {
                    String name = xmlEvent.asStartElement().getName().getLocalPart();
                    if (name.matches("[hH][1-6]")) {
                        inHeader = true;
                        sb = new StringBuilder();
                    }
                    // check name of element with: xmlEvent.asStartElement().getName().getLocalPart()
                } else if (xmlEvent.isEndElement()) {
                    String name = xmlEvent.asEndElement().getName().getLocalPart();
                    if (name.matches("[hH][1-6]")) {
                        inHeader = false;
                        result.add(sb.toString());
                    }
                } else if (xmlEvent.isCharacters()) {
                    if (inHeader) {
                        sb.append(xmlEvent.asCharacters().getData());
                    }
                }
                return true;
            }
        });

        try {
            xhtmlContent.handleXhtmlElements(page.getBodyAsString(), conversionContext, visitors);
        } catch (XhtmlException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    public static class LinkModel {
        String anchor = "";
        String title = "";
        String space = "";
        String caption = "";

        public String getAnchor() {
            return anchor;
        }

        public String getTitle() {
            return title;
        }

        public String getSpace() {
            return space;
        }

        public String getCaption() {
            return caption;
        }
    }
}